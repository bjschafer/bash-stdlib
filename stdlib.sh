#!/usr/bin/env bash
###############################################################################
#
# Single-file sourcable stdlib for bash
#
#     Usage: source stdlib.sh
#
#  Homepage: https://gitlab.com/bjschafer/bash-stdlib
#
#    Author: Braxton Schafer <braxton@cmdcentral.xyz> (bjs)
#
# Changelog:
#   - 2020-01-27 .bjs:  Initial creation
#
# License: MIT, see LICENSE for details
#
############################################################################### 

if [ "${BASH_SOURCE[0]}" -ef "$0" ]; then
    echo "Script is not meant to be executed."
    exit 1
fi

set -euo pipefail

vecho() {
    if [ -n "${VERBOSE+x}" ]; then
        echo "$@"
    fi
}

die() {
    local message="$1"
    local exitCode=$2
    echo "$message"
    if [ -n "${exitCode+x}" ]; then
        exit "$exitCode"
    else
        exit 1
    fi
}

command_exists() {
    command -v "${1+x}" >/dev/null
}

# Takes parameters like getopt(1), but ordering matters.
# parse_options <short options string> <long options string> <program name> <params>
parse_options() {
    if [ $# -eq 0 ]; then
        die "No options passed to ${FUNCNAME[0]}"
    fi
    local shortOpts="$1"
    local longOpts="$2"
    local progName="$3"
    shift 3
    declare -a params=(${@[@]})

    if command_exists getopt; then # we'll use getopt because it rocks
        local options
        options=$(getopt -o "$shortOpts" --long "$longOpts" -n "$progName" -- "${params[@]}")
        echo "$options"

    else # we'll use getopts for short options and do our best with long options
        local options
        options=$(getopts "$shortOpts" "${params[@]}")

        echo "$options"
        
    fi

}

